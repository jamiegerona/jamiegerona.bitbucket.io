'use strict';
/**
 * controllers used for contacts page
 */
app.controller('ContactsCtrl', ["$scope", "$state", "$timeout", "ContactsService",
function($scope, $state, $timeout, ContactsService) {
	$scope.init = function () {
		var params = {
			startWith: '*'
		};
		ContactsService.getContactFamilyList(params).done(function (data) {
	    	$scope.contactList = data;
			$scope.alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
	    }).fail(function () {
	    	$timeout(function () {
	    		alert('Unauthorized user. Please click the login button on Dashboard.');
	    	}, 1000);
	    });
	};

	$scope.getContactFamilyName = function (contact) {
		var val = contact.FamilyFullName;
		if (contact.FamilyFullName.indexOf('(') == -1) {
			val = contact.ClientLastName + ' (' + contact.ClientFirstName + ')';
		}
		contact.DisplayName = val;
		return val;
	};

	$scope.filterContacts = function (item) {
		if (!$scope.searchText) {
			return true;
		} else {
			var st = $scope.searchText.toLowerCase();
		    if (item.FamilyFullName.toLowerCase().indexOf(st) != -1 ||
		    	item.City.toLowerCase().indexOf(st) != -1  ||
		    	item.ContactNumber.toLowerCase().indexOf(st) != -1||
		    	item.Adviser.toLowerCase().indexOf(st) != -1 ||
		    	item.ClientType.toLowerCase().indexOf(st) != -1) {
		    	return true;
		    }
		}
	};

	$scope.getContactByStartsWith = function (letter) {
		var params = {
			startWith: letter
		};
		if ($scope.startsWithLetter == letter) {
			params.startWith = '*';
		}
		$scope.startsWithLetter = params.startWith;
		ContactsService.getContactFamilyList(params).done(function (data) {
	    	$scope.contactList = data;
	    	$scope.$apply();
	    });
	};

	$scope.selectContact = function (contactID, e) {
		$scope.selectedContact = contactID;
		e.stopPropagation();
	};

	$scope.viewContactInfo = function (contact) {
		$state.get("app.contacts").Obj = contact;
		$state.go("app.contacts");
	};

	$scope.init();
}]);

app.controller('ContactInfoCtrl', ["$scope", "$rootScope", "$state", "$stateParams", "$location", "ContactsService",
function($scope, $rootScope, $state, $stateParams, $location, ContactsService) {
	$scope.contact = $state.get('app.contacts').Obj;
	if (!$scope.contact) {
		$location.path('app/contacts');
	}
	$rootScope.contactName = $scope.contact.DisplayName;
	$scope.init = function () {
		var familyId = $stateParams.id;
		ContactsService.getContactFamilyInfo(familyId).done(function (data) {
	    	$scope.contactInfo = data;
	    });
	};

	$scope.toggleMenu = function () {
		$scope.showTabs = !$scope.showTabs;
	};

	$scope.tabs = [
		{
			title: 'SUMMARY',
			content: 'assets/views/summary.html'
		},
		{
			title: 'CLIENTS',
			content: 'assets/views/client_tab.html'
		},
		{
			title: 'LOANS'
		},
		{
			title: 'INSURANCE'
		},
		{
			title: 'FINANCIALS'
		},
		{
			title: 'WORKFLOW'
		},
		{
			title: 'RECORDS'
		}
	];

	// Mock data
	$scope.clientLoans = [
		{
			amount: '$120,000',
			loanee: 'Cristie',
			loaner: 'Wespac'
		},
		{
			amount: '$120,000',
			loanee: 'Cristie',
			loaner: 'Wespac'
		}
	];

	$scope.exisitingBenefits = [
		{
			name: "Trauma Cover",
			amount: "$68,000",
			insurer: "amp"
		},
		{
			name: "Car Accident",
			amount: "$55,000 Annually",
			insurer: "anz"
		},
		{
			name: "Disability",
			amount: "$68,000",
			insurer: "alife"
		}
	];

	$scope.activeTasks = [
		{
			name: 'Meeting',
			status: 'Due Today',
			desc: 'Set meeting for Insurance Application',
			owner: 'James Punnet'
		},
		{
			name: 'Submit Report',
			status: 'Due Today',
			desc: 'Send Details to Loan Market',
			owner: 'Claurence Marketer'
		}
	];

	$scope.recentActivities = [
		{
			name: 'Follow up email sent',
			date: '1 Aug',
			desc: 'Email sent to <span>c.adams@gmail.com</span>',
			icon: 'message'
		},
		{
			name: '1 Task Completed',
			date: '29 Jul',
			desc: 'This is a sample of a name task',
			icon: 'check'
		}
	];

	$scope.notes = [
		{
			name: 'NOTE HEADING SAMPLE',
			desc: 'Sea fifth was grass seasons one rule. Beginning earth them bearing.'
		}
	];
	$scope.init();
}]);

app.controller('ClientCtrl', ["$scope", "$rootScope", "$state", "$stateParams", "$uibModal", "ContactsService",
function($scope, $rootScope, $state, $stateParams, $uibModal, ContactsService) {
	$scope.contact = $state.get('app.contacts').Obj;
	$rootScope.contactName = $scope.contact.DisplayName;
	$scope.init = function () {
		var familyId = $stateParams.id;
		ContactsService.getClientInfo(familyId).done(function (data) {
	    	$scope.clients = data;
	    });
	};

	$scope.open = function (size) {
        var modalInstance = $uibModal.open({
            templateUrl: 'assets/views/client_relationship.html',
            controller: 'ClientRelationshipCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        });
    };
	$scope.init();
}]);

app.controller('ClientRelationshipCtrl', ["$scope", "$uibModalInstance", "ContactsService",
function($scope, $uibModalInstance,  ContactsService) {
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);