'use strict';

app.factory('ContactsService', function () {
  return {
    getContactFamilyList: function (params) {
      return API.getContactFamilyList(params);
    },
    getContactFamilyInfo: function (familyId) {
      return API.getContactFamilyInfo(familyId);
    },
    getClientInfo: function (familyId) {
      return API.getClientInfo(familyId);
    }
  }
});