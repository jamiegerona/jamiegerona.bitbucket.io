'use strict';

window.API = (function() {

    var baseUrl = 'https://testapi.nzfsg.co.nz/',

        contentType = 'application/x-www-form-urlencoded',

        url = {
        	LOGIN: 'login',
        	GET_CONTACT_FAMILY_LIST: 'contacts/FamilyListGet',
        	GET_CONTACT_FAMILY_INFO: 'contacts/ContactFamilyInfoGet?familyId=',
        	GET_CLIENT_INFO: 'contacts/ClientInformGet?familyId='
        },

        token,

        self = {
        	getToken: function () {
        		var name = "token=";
			    var decodedCookie = decodeURIComponent(document.cookie);
			    var ca = decodedCookie.split(';');
			    for(var i = 0; i <ca.length; i++) {
			        var c = ca[i];
			        while (c.charAt(0) == ' ') {
			            c = c.substring(1);
			        }
			        if (c.indexOf(name) == 0) {
			            return c.substring(name.length, c.length);
			        }
			    }
			    return "";
        	},

            request: function (options) {
                if (!options.headers) {
                    options.headers = {};
                }

                if (!options.headers['Authorization']) {
                    options.headers['Authorization'] = 'Bearer ' + self.getToken();
                }

                // Apply the content type
                if (!options['contentType']) {
                    options['contentType'] = contentType;
                }

                return $.ajax(options);
            },

            login: function () {
            	var options = {
                    type: 'POST',
                    url: baseUrl + url.LOGIN,
                    contentType: contentType,
                    data: {
                    	username: 'testuser1@loanmarket.co.nz',
                    	password: 'test345'
                    }
                };
                
                $.ajax(options).done(function(data) {
                	document.cookie = "token=" + data;
                	alert("You have successfully logged in!");
                });
            },

            getContactFamilyList: function (params) {
            	params.byPassFilter = true;
            	var options = {
                    'type': 'GET',
                	'url': baseUrl + url.GET_CONTACT_FAMILY_LIST,
                	'data': params
                };
                return self.request(options);
			},

            getContactFamilyInfo: function (familyId) {
            	var options = {
                    'type': 'GET',
                	'url': baseUrl + url.GET_CONTACT_FAMILY_INFO + familyId
                };
                return self.request(options);
			},

            getClientInfo: function (familyId) {
            	var options = {
                    'type': 'GET',
                	'url': baseUrl + url.GET_CLIENT_INFO + familyId
                };
                return self.request(options);
			}
        };

    return self;
})();